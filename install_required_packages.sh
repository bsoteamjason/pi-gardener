#!/bin/bash

# Installs the required system packages needed by our application.
# Based on https://pimylifeup.com/raspberry-pi-humidity-sensor-dht22/

# Get the latest and greatest.
sudo apt-get update
sudo apt-get upgrade

# Install the python language and libraries for our monitoring application.
sudo apt-get install python3-dev python3-pip
sudo python3 -m pip install --upgrade pip setuptools wheel
sudo pip3 install Adafruit_DHT

# Install our web server.
sudo apt-get install apache2

